﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KleinAttackCode
{
    public class RC4Utils
    {
        private const int N = 256;

        private Byte[] RC4KeyScheduling(byte [] key)
        {
            int l = key.Length;

            Byte[] S = new Byte[N];

            for (int i = 0; i < N; i++)
            {
                S[i] = (Byte)i;
            }

            uint j = 0;

            for (uint i = 0; i < N; i++)
            {
                j = (uint)((j + S[i] + key[i % l]) % N);
                Byte temp = S[i];
                S[i] = S[j];
                S[j] = temp;
            }
            return S;
        }

        public byte[] Encrypt(string clearText, string key)
        {
            byte[] S = RC4KeyScheduling(str2Bytes(key));
            byte[] clearBytes = str2Bytes(clearText);

            IList<Byte> output = new List<Byte>();

            uint i = 0;
            uint j = 0;

            for (int count = 0; count < clearBytes.Length; count++)
            {
                i = (++i) % N;
                j = (j + S[i]) % N;
                Byte temp = S[i];
                S[i] = S[j];
                S[j] = temp;
                byte K = S[(S[i] + S[j]) % N];
                byte outputByte = (byte)(clearBytes[count] ^ K);
                output.Add(outputByte);
            }
            return output.ToArray();
        }

        public string TestEncrypt(string clearText, string key)
        {
            byte[] output = Encrypt(clearText, key);
            return bin2hex(bytes2Str(output));
        }

        private byte[] str2Bytes(string value)
        {
            byte[] bytes = Encoding.GetEncoding(1252).GetBytes(value);
            return bytes;
        }

        private string bytes2Str(byte[] value)
        {
            String str = Encoding.GetEncoding(1252).GetString(value);
            return str;
        }

        private string bin2hex(string bindata)
        {
            int i;
            byte[] bytes = Encoding.GetEncoding(1252).GetBytes(bindata);
            string hexString = "";
            for (i = 0; i < bytes.Length; i++)
            {
                if(i > 0)
                {
                    hexString += "-";
                }
                hexString += bytes[i].ToString("x2");
            }
            return hexString.ToUpper();
        }

        public string Decrypt(byte[] encryptedBytes, string key)
        {
            byte[] decrypted = Encrypt(bytes2Str(encryptedBytes), key);
            return bytes2Str(decrypted);
        }
    }
}
