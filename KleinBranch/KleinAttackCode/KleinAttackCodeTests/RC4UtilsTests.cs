﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using KleinAttackCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
namespace KleinAttackCode.Tests
{
    [TestClass()]
    public class RC4UtilsTests
    {
        [TestMethod()]
        public void EncryptTest()
        {
            String clar = "Clar";
            String key = "123";
            byte[] expectedBytes = { 0x10, 0x9C, 0xDE, 0xF0 };
            RC4Utils rc4Utils = new RC4Utils();
            byte[] result = rc4Utils.Encrypt(clar, key);
            Assert.AreEqual(Encoding.GetEncoding(1252).GetString(result), Encoding.GetEncoding(1252).GetString(expectedBytes));
        }

        [TestMethod()]
        public void TestEncryptTest()
        {
            String clar = "Clar";
            String key = "123";
            String expectedText = "10-9C-DE-F0";
            RC4Utils rc4Utils = new RC4Utils();
            String result = rc4Utils.TestEncrypt(clar, key);
            Assert.AreEqual(result, expectedText);
        }

        [TestMethod()]
        public void DecryptTest()
        {
            byte[] encrypted = { 0x10, 0x9C, 0xDE, 0xF0 };
            String key = "123";
            String expectedText = "Clar";
            RC4Utils rc4Utils = new RC4Utils();
            String result = rc4Utils.Decrypt(encrypted, key);
            Assert.AreEqual(result, expectedText);
        }

        [TestMethod()]
        public void LoadTestEncrypt()
        {
            Stopwatch watch = new Stopwatch();
            String text = System.IO.File.ReadAllText(@"C:\Facultate\proiect-taip\KleinBranch\KleinAttackCode\bin\Debug\test_file.txt");
            String key = "123";
            RC4Utils rc4utils = new RC4Utils();
            watch.Start();
            String result = rc4utils.TestEncrypt(text, key);
            watch.Stop();
            Console.WriteLine("Time elapsed: {0}", watch.Elapsed);
            Assert.IsTrue(watch.ElapsedMilliseconds < 500);
        }
    }
}