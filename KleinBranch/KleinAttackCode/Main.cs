﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KleinAttackCode
{
    public class KleinCode
    {
        
        public static void Main()
        {
            String clar = "Clar";
            String key = "123";

            RC4Utils rc4Utils = new RC4Utils();

            Console.WriteLine("Encrypt: " + clar + " using key: " + key);

            String result = rc4Utils.TestEncrypt(clar, key);

            Console.WriteLine("Cyphertext: " + result);

            String result2 = rc4Utils.Decrypt(rc4Utils.Encrypt(clar, key), key);
            Console.WriteLine("To string : " + rc4Utils.Encrypt(clar, key).ToString());
            Console.WriteLine("Decrypted: " + result2);

            Console.ReadKey();
        }
    }
}
