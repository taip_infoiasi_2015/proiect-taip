﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Encryption
{
    public class Init
    {
        private byte[] key;
        private int v;

        public Init(int v)
        {
            this.v = v;
        }

        public Init(byte[] k)
        {
            this.key = k;
        }

        public byte[] getKey()
        {
            return key;
        }

        public void setKey(byte[] key)
        {
            this.key = key;
        }

    }
}
