﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Encryption
{
    public class Context 
    {
        private ActionStrategy actionStrategy;
        private string data;
        private string pwd;
        private bool ispwdHex;

        public ActionStrategy ActionStrategy
        {
            get
            {
                return null; 
            }
            set
            {
            }
        }

        public void SetStrategy(ActionStrategy actionstrategy)
        {
            this.actionStrategy = actionstrategy;
        }

        public void Call()
        {
            actionStrategy.CallAlgorithm( pwd,  data,  ispwdHex);
        }

     
    }
}
