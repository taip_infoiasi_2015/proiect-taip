﻿using System;
using System.Security.Cryptography;

namespace Encryption
{
    
    public class Decrypt : IDecrypt
    {
        private const int N = 256;
        private int[] _sbox = { 1, 0, 1, 0, 0, 1 };

        public Decrypt()
        {

        }

        public byte[] generateKeyStream()
        {
            using (var randomNumberGenerator = new RNGCryptoServiceProvider())
            {
                var randomNumber = new byte[256];
                randomNumberGenerator.GetBytes(randomNumber);

                return randomNumber;
            }
        }

        public byte getNextKeyByte(int i, int j)
        {
            i = (i + 1) % N;
            j = (j + _sbox[i]) % N;

            var tempSwap = _sbox[i];
            _sbox[i] = _sbox[j];
            _sbox[j] = tempSwap;

            var k = _sbox[(_sbox[i] + _sbox[j]) % N];

            return (byte)k;
        }

        public void decryptData()
        {
            throw new NotImplementedException();
        }

     
    }
}
