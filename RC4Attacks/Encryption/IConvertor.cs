﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Encryption
{
    public interface IConvertor
    {
         int ord(char ch);
         char chr(int i);
         string pack(string packtype, string datastring);
         string bin2hex(string bindata);
    }
}
