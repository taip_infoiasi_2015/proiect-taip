﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace Encryption
{

    public abstract class ActionStrategy
    {

        public abstract void CallAlgorithm(string pwd, string data, bool ispwdHex);

    }
}
