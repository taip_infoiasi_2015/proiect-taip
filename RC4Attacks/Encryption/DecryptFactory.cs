﻿
namespace Encryption
{
   
    public class DecryptFactory
    {
        public Decrypt getKey(string size)
        {
            if (size.Length > 0)
                return new Decrypt();
            return null;
        }
    }
}
