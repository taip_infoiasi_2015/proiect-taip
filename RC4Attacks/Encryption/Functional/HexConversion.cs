﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Encryption.Functional
{
    public static class HexConversion 
    {

        public static string ToHex(this byte b)
        {
            return b.ToString("X2");
        }

        public static string ToHex(this byte[] b)
        {
            var sb = new StringBuilder(b.Length);
            var upperBound = b.GetUpperBound(0);

            for (int i = b.GetLowerBound(0); i <= upperBound; i++)
                sb.Append(b[i].ToHex());

            return sb.ToString();
        }

        public static string ToUTF8Hex(this string s)
        {
            return s.ToUTF8().ToHex();
        }

        public static byte[] FromHex(this string s)
        {
            if (s.Length % 2 != 0)
                throw new ArgumentException("Length must be even");

            var bb = new byte[s.Length >> 1];

            for (int i = 0, j = 0; i < s.Length; i += 2, j++)
            {
                bb[j] = Byte.Parse(s.Substring(i, 2), System.Globalization.NumberStyles.HexNumber);
            }

            return bb;
        }

        public static string FromUTF8Hex(this string s)
        {
            return s.FromHex().FromUTF8();

        }


    }
}
