﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Encryption
{
    public enum LoadMethod
    {
        DataFileKeyScreen = 10,
        DataScreenKeyFile = 20,
        DataScreenKeyScreen = 30,
        DataFileKeyFile = 40

    }
}
