﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Encryption
{
    public class LoadFile : ILoadFile
    { 
        private string filePath = string.Empty;

        public LoadFile(string filePath)
        {
            this.filePath = filePath;
        }

        public string[] LoadFromFile()
        {
            string line;
            List<string> entries = new List<string>();
            
            try
            {
                StreamReader theReader = new StreamReader(filePath, Encoding.Default);

                using (theReader)
                {

                    do
                    {
                        line = theReader.ReadLine();

                        if (line != null)
                        {
                            entries.Add(line);
                        }

                    }
                    while (line != null);

                    theReader.Close();
                }
            }

            catch (Exception e)
            {
                throw e;
            }

            return entries.ToArray();
        }
        }
}
