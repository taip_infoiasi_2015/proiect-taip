﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Encryption
{
    public class LoadDecider
    {
        public string GetLoadMethod(LoadMethod method)
        {
            ILoadMode mode = null;
            switch (method)
            {
                case LoadMethod.DataFileKeyFile:
                    mode = new LoadDataFileKeyFile();
                    break;
                case LoadMethod.DataFileKeyScreen:
                    mode = new LoadDataFileKeyScreen();
                    break;
                case LoadMethod.DataScreenKeyFile:
                    mode = new LoadDataScreenKeyFile();
                    break;
                case LoadMethod.DataScreenKeyScreen:
                    mode = new LoadDataScreenKeyScreen();
                    break;
                default:
                    mode = new LoadDataFileKeyFile();
                    break;
            }

            return string.Empty;
        }


    }
}
