﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Encryption
{
    public interface ILoadScreen
    {
        string LoadFromScreen();
    }
}
