﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Encryption
{
    public class LoadDecider
    {
        public void GetLoadMethod(int loadMethod)
        {
            ILoadMode mode = null;
            switch (loadMethod)
            {
                case 1:
                    mode = new LoadModeFile();
                    break;
                case 2:
                    mode = new LoadModeScreen();
                    break;
                default:
                    mode = new LoadModeFile();
                    break;
            }
        }
    }
}
