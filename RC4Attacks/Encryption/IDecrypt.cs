﻿using System;

namespace Encryption
{
    public interface IDecrypt
    {
        byte[] generateKeyStream();
        byte getNextKeyByte(int a, int b);
        void decryptData();
    }
}
