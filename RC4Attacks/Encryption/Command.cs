﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Encryption
{
    public class Command
    {

        LoadMethod loadMethod;
        string encryptedText = string.Empty;

        public void CreateInstance(LoadMethod method)
        {
            LoadDecider decider = new LoadDecider();
            encryptedText = decider.GetLoadMethod(method);
        }
    }
}
