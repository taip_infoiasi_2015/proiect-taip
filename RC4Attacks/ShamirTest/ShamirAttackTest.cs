﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ro.infoiasi.taip.attacks;
using ro.infoiasi.taip.control;
using RC4Attacks;
using TypeException;
using KeySizeException;
using System.IO;
using FileEmptyException;
using TestVerification;


namespace ShamirTest
{
    [TestClass]
    public class ShamirAttackTest
    {
        [TestMethod]
        [ExpectedException(typeof(TypeException.TypeException))]   
        public void TestMethod_CheckExtension()
        {
            var ts = new TestVerificationClass();
            ts.checkFile("test.txt");

        }

        [TestMethod]
        [ExpectedException(typeof(FileEmptyException.FileEmptyException))]
        public void TestMethod_CheckFileSize()
        {
            var ts = new TestVerificationClass();
            ts.checkFile("asd.cap");
        }

        [TestMethod]
        public void TestMethod_CheckPassword()
        {
            var ts = new TestVerificationClass();
            Assert.AreEqual(ts.checkPassword("asdasdsa"), true);
        }

        [TestMethod]
        public void TestMethod_CheckSuccessOfAttack()
        {
            var ts = new ShamirAttack();
            Assert.AreEqual(ts.checkSuccessOfAttack("asdasda"), "Success");
        }
    }
}
