﻿using ro.infoiasi.taip.control;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ro.infoiasi.taip.attacks
{
    abstract class EventAbstract
    {
        private List<IObserver> _events = new List<IObserver>();

        public delegate void StatusUpdate();
        public event StatusUpdate OnStatusUpdate = null;

        public void Attach(IObserver eventt)
        {
            _events.Add(eventt);
        }

        public void Detach(IObserver eventt)
        {
            _events.Remove(eventt);
        }

        public void Attach2(IObserver eventt)
        {
            //For way 2 lets assign attach the observers with subjects
            OnStatusUpdate += new StatusUpdate(eventt.Update);
        }
        public void Detach2(IObserver eventt)
        {
            //For way 2 lets assign detach the observers with subjects
            OnStatusUpdate -= new StatusUpdate(eventt.Update);
        }

        public void Notify()
        {
            foreach (IObserver eventt in _events)
                    eventt.Update();
        }
    }
}

