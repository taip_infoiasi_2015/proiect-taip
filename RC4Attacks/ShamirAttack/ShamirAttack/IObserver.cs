﻿using ro.infoiasi.taip.attacks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ro.infoiasi.taip.control
{
    public interface IObserver
    {
        // Operations
        void register();
        void execute();
        void Update();

    } /* end interface IObserver */

}
