﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ro.infoiasi.taip.control;

namespace ro.infoiasi.taip.attacks
{
    public interface IAttackStrategy
    {

         void loadData(String encryptedStream, int sessionId);
         void exportData();
         void registerObserver(IObserver observer);     
    }
}
