﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ro.infoiasi.taip.control
{

    public class MainObserver : IObserver
    {
        public string ObserverName { get; private set; }

        public MainObserver(string name)
        {
            this.ObserverName = name;
        }

        public void Update()
        {
            Console.WriteLine("{0}: A new event has happened",this.ObserverName);
        }

        public void register()
        {
            
        }

        public void execute()
        {
           
        }
    }
    /* end class MainObserver */
}
