﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShamirAttack
{
    public abstract class AttackTarget
    {
        private string _id;

        // Constructor
        public AttackTarget(string id)
        {
            this._id = id;
        }

        // Gets id
        public string Id
        {
            get { return _id; }
        }

        public abstract AttackTarget Clone();
    }


    public class AttackonPT : AttackTarget
    {
        // Constructor
        public AttackonPT(string id)
          : base(id)
        {
        }

        // Returns a shallow copy
        public override AttackTarget Clone()
        {
            return (AttackTarget)this.MemberwiseClone();
        }
    }


    public class AttackonWEP : AttackTarget
    {
        // Constructor
        public AttackonWEP(string id)
          : base(id)
        {
        }

        // Returns a shallow copy
        public override AttackTarget Clone()
        {
            return (AttackTarget)this.MemberwiseClone();
        }
    }
}

