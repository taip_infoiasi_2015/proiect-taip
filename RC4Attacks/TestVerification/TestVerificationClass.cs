﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TypeException;
using FileEmptyException;
using System.IO;
//using RC4Attacks;

namespace TestVerification
{
    public class TestVerificationClass
    {
        public void checkFile(string fileName)
        {
            FileInfo info = new FileInfo(fileName);
        
            if (info.Extension != ".cap" && info.Extension != ".pcap" && info.Extension != ".ivs" && info.Extension != ".dump") throw new TypeException.TypeException();
            //check is filepackage is empty
            else 
                if (info.Length == 0) throw new FileEmptyException.FileEmptyException();
        }

        public int checkIfTextIsHex(string text) //check if the received text as passw is in hex
        {
            char[] txt = text.ToCharArray();
            int k = -1;

            for (int i = 0; i < txt.Length; i++)
                if (txt[i] >= '0' && txt[i] <= 'F')
                    k = 1;
                else k = -1;

            if (k == 1) return 1;
            else return 0;
        }

        public bool checkPassword(string pass)
        {
            if (pass.Length != 128)
                return false;
            if (checkIfTextIsHex(pass) != 1)
                return false;
            return true;
        }

    }
}
