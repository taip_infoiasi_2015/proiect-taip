﻿using RC4Service.Functional;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RC4Service
{
    public class Rc4ED
    {
         public const int StateSize = 256;
        public const int StateModuloMask = StateSize - 1;

        public static byte[] BlankState = InitState();

        public byte[] State = new byte[StateSize];

        public int StateX = 0;
        public int StateY = 0;
        public Rc4ED()
            : base()
        {
            BlankState.Copy(State, StateSize);
        }
       
        public Rc4ED(byte[] key)
            : this()
        {

            int keyLowerBound = key.GetLowerBound(0);
            int keyUpperBound = key.GetUpperBound(0);
            int keyIndex = keyLowerBound;

            for (int i, j = i = 0; i <= 255; i++)
            {
                j = (j + State[i] + key[keyIndex]) & StateModuloMask;
                State.Swap(i, j);

                if (++keyIndex > keyUpperBound)
                    keyIndex = 0;

            }
        }

        public Rc4ED(string key)
            : this(key.ToUTF8())
        {
        }

        public void Encrypt(byte[] b)
        {
            Encrypt(b, b.GetLowerBound(0), b.GetUpperBound(0) - b.GetLowerBound(0) + 1);
        }
      
        public void Encrypt(byte[] b, int index, int count)
        {
            int i = StateX;
            int j = StateY;

            byte x, y;

            for (; --count >= 0;)
            {
                i = ++i & StateModuloMask;
                j = (j + State[i]) & StateModuloMask;

                x = State[i];
                State[i] = y = State[j];
                State[j] = x;

                b[index++] ^= State[((int)x + (int)y) & StateModuloMask];
            }


            StateX = i;
            StateY = j;
        }

        public byte[] Encrypt(string s)
        {
            var bb = s.ToUTF8();
            Encrypt(bb);

            return bb;
        }
      
        public const int BufferSize = 1024 * 8;

        public void Encrypt(Stream streamIn, Stream streamOut)
        {
            byte[] buf = new byte[BufferSize];

            int nRead;

            while ((nRead = streamIn.Read(buf, 0, BufferSize)) > 0)
            {
                Encrypt(buf, 0, nRead);
                streamOut.Write(buf, 0, nRead);
            }
            streamOut.Flush();

        }

        public static byte[] InitState()
        {
            var state = new byte[StateSize];
            for (int i = StateSize; --i >= 0;)
                state[i] = (byte)i;

            return state;
        }


    }
}
