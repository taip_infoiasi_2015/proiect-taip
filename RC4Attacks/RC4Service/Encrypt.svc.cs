﻿using RC4Service.Functional;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace RC4Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : EncryptTranslation
    {
        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }


        public string EncryptData(string key, string source)
        {
            byte[] byteKey = GetByte(key);
            byte[] byteSource = GetByte(source);

            return Encrypt(byteKey, byteSource);
        }

        public string DecryptData(string key, string source)
        {
            byte[] byteKey = GetByte(key);
            byte[] byteSource = GetByteFromHex(source);

            return Encrypt(byteKey, byteSource);
        }

        private string Encrypt(byte[] key, byte[] source)
        {
            bool InSteps = false;
            var rc4 = new Rc4ED(key);

            if (!InSteps)
                rc4.Encrypt(source);
            else
            {
                for (int i = 0; i < source.Length; i++)
                    rc4.Encrypt(source, i, 1);
            }

            return source.ToHex();

        }

        private byte[] GetByte(string text)
        {
            return text.ToUTF8Hex().FromHex();
        }

        private byte[] GetByteFromHex(string text)
        {
            return text.FromHex();
        }


    }
}
