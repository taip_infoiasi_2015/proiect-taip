﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RC4Service.Functional
{
    public static class ExtensionMethods_Array
    {

        public static void Copy<T>(this T[] src, T[] dest, int count)
        {
            Array.Copy(src, dest, count);
        }

        public static void Copy<T>(this T[] src, int srcIndex, T[] dest, int destIndex, int count)
        {
            Array.Copy(src, srcIndex, dest, destIndex, count);
        }

        public static void Swap<T>(this T[] array, int index1, int index2)
        {
            T tmp;
            tmp = array[index1];
            array[index1] = array[index2];
            array[index2] = tmp;
        }
       
    }
}
