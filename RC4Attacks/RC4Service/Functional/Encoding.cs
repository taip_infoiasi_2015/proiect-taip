﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RC4Service.Functional
{
    public static class Encoding
    {

        public static byte[] ToUTF8(this char c)
        {
            return System.Text.Encoding.UTF8.GetBytes(new char[] { c });
        }

        public static byte[] ToUTF8(this char[] c)
        {
            return System.Text.Encoding.UTF8.GetBytes(c);
        }

        public static byte[] ToUTF8(this char[] c, int index, int count)
        {
            return System.Text.Encoding.UTF8.GetBytes(c, index, count);
        }

        public static byte[] ToUTF8(this string s)
        {
            return System.Text.Encoding.UTF8.GetBytes(s);
        }

        public static byte[] ToUTF8(this string s, int index, int count)
        {
            return System.Text.Encoding.UTF8.GetBytes(s.Substring(index, count));
        }

        public static string FromUTF8(this byte[] b)
        {
            return System.Text.Encoding.UTF8.GetString(b);
        }

        public static string FromUTF8(this byte[] b, int index, int count)
        {
            return System.Text.Encoding.UTF8.GetString(b, index, count);
        }


        public static string FromHexString(string HexValue)
        {

            string StrValue = "";
            while (HexValue.Length > 0)
            {
                StrValue += System.Convert.ToChar(System.Convert.ToUInt32(HexValue.Substring(0, 2), 16)).ToString();
                HexValue = HexValue.Substring(2, HexValue.Length - 2);
            }
            return StrValue;
        }
    }
}
