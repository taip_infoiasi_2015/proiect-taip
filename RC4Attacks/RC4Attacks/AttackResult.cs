﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ro.infoiasi.taip.models
{
    public class AttackResult
    {
        //private int sessionId { get; set; }
        //private int time { get; set; }

        public int sessionId { get; set; }
        public int time { get; set; }

        public AttackResult(int sessionId, int time)
        {
            this.sessionId = sessionId;
            this.time = time;
        }
    }
}
