﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ro.infoiasi.taip.models
{
    class Model
    {
        private static Model instance;
        private string sourceStream { get; set; }
        private string encryptedStream { get; set; }
        private AttackResult attackResult { get; set; }

        private Model()
        {

        }

        public static Model getInstance()
        {
            if (null == instance)
            {
                instance = new Model();
            }
            return instance;
        }
    }
}
