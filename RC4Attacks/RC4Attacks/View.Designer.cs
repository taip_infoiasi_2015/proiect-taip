﻿namespace RC4Attacks
{
    partial class View
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbFilenames = new System.Windows.Forms.TextBox();
            this.LoadFile = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbFilenames
            // 
            this.tbFilenames.AllowDrop = true;
            this.tbFilenames.Location = new System.Drawing.Point(12, 31);
            this.tbFilenames.Name = "tbFilenames";
            this.tbFilenames.Size = new System.Drawing.Size(424, 20);
            this.tbFilenames.TabIndex = 3;
            // 
            // LoadFile
            // 
            this.LoadFile.Location = new System.Drawing.Point(459, 29);
            this.LoadFile.Name = "LoadFile";
            this.LoadFile.Size = new System.Drawing.Size(75, 23);
            this.LoadFile.TabIndex = 2;
            this.LoadFile.Text = "Incarca";
            this.LoadFile.UseVisualStyleBackColor = true;
            this.LoadFile.Click += new System.EventHandler(this.button1_Click);
            // 
            // View
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(638, 322);
            this.Controls.Add(this.tbFilenames);
            this.Controls.Add(this.LoadFile);
            this.Name = "View";
            this.Text = "View";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbFilenames;
        private System.Windows.Forms.Button LoadFile;
    }
}