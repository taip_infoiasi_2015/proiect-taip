﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ro.infoiasi.taip.attacks;
using TestVerification;
using System.IO;

namespace RC4Attacks
{
    public partial class View : Form
    {
        public View()
        {
            InitializeComponent();
        }
        private string lastDirectory = ".";
        private void Form1_Load(object sender, EventArgs e)
        {
            IAttackFactory factory = AttackFactory.getInstance();
        }

        private string FileDialog(string Filter, int FilterIndex, bool multipleFiles, string separator, string initDirectory)
        {
            string fileseparator = separator;
            string filenames = "";
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = Filter;
            ofd.FilterIndex = FilterIndex;
            ofd.InitialDirectory = initDirectory;
            ofd.Multiselect = multipleFiles;
            ofd.RestoreDirectory = true;
            ofd.DereferenceLinks = true;
            ofd.CheckPathExists = true;
            ofd.CheckFileExists = true;

            if (string.IsNullOrEmpty(fileseparator))
                fileseparator = " ";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                foreach (string filename in ofd.FileNames)
                {
                    filenames += fileseparator;

                    if (filename.Contains(" "))
                        filenames += "\"" + filename + "\"";
                    else
                        filenames += filename;
                }

                // Save last directory
                if (ofd.FileNames.Length > 0)
                {
                    this.lastDirectory = System.IO.Path.GetDirectoryName(ofd.FileNames[ofd.FileNames.Length - 1]);
                }
            }
            return filenames;
        }


        void UpdateView()
        {
            throw new NotImplementedException();

        }

        private string FileDialog(string Filter, int FilterIndex, bool multipleFiles, string separator)
        {
            return FileDialog(Filter, FilterIndex, multipleFiles, separator, ".");
        }

        public string resultFilename = "";

        private void button1_Click(object sender, EventArgs e)
        {
            string captureFileExtensions = "Capture files (*.cap, *.pcap, *.ivs, *.dump)|*.cap;*.pcap;*.ivs;*.dump";
            resultFilename = this.FileDialog(captureFileExtensions, 0, true, null, this.lastDirectory);
            //System.IO.Path.GetFullPath
            this.tbFilenames.Text += " " + resultFilename.Trim();
            this.tbFilenames.Text = this.tbFilenames.Text.Trim();

            TestVerificationClass ts = new TestVerificationClass();
            ts.checkFile(this.tbFilenames.Text);
            MessageBox.Show(ts.checkPassword("jssjhbfdj").ToString());

        }

        public string getFile()
        {
            return resultFilename;
        }
    }
}
