﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ro.infoiasi.taip.attacks
{
    
    public sealed class AttackFactory : IAttackFactory
    {

        private static IAttackFactory instance;
        private AttackFactory()
        {
            
        }
        public static IAttackFactory getInstance()
        {
            if(null == instance)
            {
                instance = new AttackFactory();
            }
            return instance;
        }

        public IAttackFactory createAttack(String attackName)
        {
            switch (attackName) {
                case "klein":
                    {
                        break;
                    }
                case "shamir":
                    {
                        break;
                    }
            }
            return null;
        }

    }
}
