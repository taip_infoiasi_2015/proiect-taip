﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientInterface
{
    public partial class RC4 : Form
    {
        public RC4()
        {
            InitializeComponent();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Text files | *.txt"; 
            dialog.Multiselect = false; 
            if (dialog.ShowDialog() == DialogResult.OK) 
            {
                String path = dialog.FileName; 
                label2.Text =  Path.GetFileName(path); ;
                var load = new Encryption.LoadFile(path);
                string[] result = load.LoadFromFile();
                plaintext.Text = String.Join(Environment.NewLine, result);
            }
        }

        private void encryption_button_Click(object sender, EventArgs e)
        {
            byte[] ba = Encoding.Default.GetBytes(this.key.Text);
            var hexString = BitConverter.ToString(ba);
           

            hexString = hexString.Replace("-", "");

            var encryption = new Encryption.Encryption();

            criptotext.Text = encryption.Encrypt(hexString, plaintext.Text, true);

        }

        private void key_TextChanged(object sender, EventArgs e)
        {
            this.encryption_button.Enabled = !string.IsNullOrWhiteSpace(this.key.Text);
        }

        private void decrypt_button_Click(object sender, EventArgs e)
        {
            byte[] ba = Encoding.Default.GetBytes(this.key.Text);
            var hexString = BitConverter.ToString(ba);


            hexString = hexString.Replace("-", "");
            var encryption = new Encryption.Encryption();
            original_text.Text = encryption.Decrypt(hexString, criptotext.Text, true);
        }

        private void RC4_Load(object sender, EventArgs e)
        {

        }
    }
}
