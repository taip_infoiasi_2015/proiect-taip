﻿namespace Interface
{
    partial class RC4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.plaintext = new System.Windows.Forms.TextBox();
            this.encryption_button = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.criptotext = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.key = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.original_text = new System.Windows.Forms.TextBox();
            this.decrypt_button = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(23, 24);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(85, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Choose File";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(114, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(120, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "label2";
            // 
            // plaintext
            // 
            this.plaintext.Location = new System.Drawing.Point(183, 27);
            this.plaintext.Multiline = true;
            this.plaintext.Name = "plaintext";
            this.plaintext.ReadOnly = true;
            this.plaintext.Size = new System.Drawing.Size(331, 188);
            this.plaintext.TabIndex = 4;
            // 
            // encryption_button
            // 
            this.encryption_button.Enabled = false;
            this.encryption_button.Location = new System.Drawing.Point(10, 34);
            this.encryption_button.Name = "encryption_button";
            this.encryption_button.Size = new System.Drawing.Size(75, 23);
            this.encryption_button.TabIndex = 5;
            this.encryption_button.Text = "Encrypt";
            this.encryption_button.UseVisualStyleBackColor = true;
            this.encryption_button.Click += new System.EventHandler(this.encryption_button_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.criptotext);
            this.groupBox1.Controls.Add(this.encryption_button);
            this.groupBox1.Location = new System.Drawing.Point(23, 261);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(381, 250);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Encryption";
            // 
            // criptotext
            // 
            this.criptotext.Location = new System.Drawing.Point(111, 34);
            this.criptotext.Multiline = true;
            this.criptotext.Name = "criptotext";
            this.criptotext.ReadOnly = true;
            this.criptotext.Size = new System.Drawing.Size(248, 190);
            this.criptotext.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(536, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Encryption Key";
            // 
            // key
            // 
            this.key.Location = new System.Drawing.Point(643, 34);
            this.key.Multiline = true;
            this.key.Name = "key";
            this.key.Size = new System.Drawing.Size(375, 66);
            this.key.TabIndex = 8;
            this.key.TextChanged += new System.EventHandler(this.key_TextChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.original_text);
            this.groupBox2.Controls.Add(this.decrypt_button);
            this.groupBox2.Location = new System.Drawing.Point(550, 261);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(425, 250);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Decryption";
            // 
            // original_text
            // 
            this.original_text.Location = new System.Drawing.Point(148, 34);
            this.original_text.Multiline = true;
            this.original_text.Name = "original_text";
            this.original_text.ReadOnly = true;
            this.original_text.Size = new System.Drawing.Size(248, 190);
            this.original_text.TabIndex = 1;
            // 
            // decrypt_button
            // 
            this.decrypt_button.Location = new System.Drawing.Point(7, 34);
            this.decrypt_button.Name = "decrypt_button";
            this.decrypt_button.Size = new System.Drawing.Size(75, 23);
            this.decrypt_button.TabIndex = 0;
            this.decrypt_button.Text = "Decrypt";
            this.decrypt_button.UseVisualStyleBackColor = true;
            this.decrypt_button.Click += new System.EventHandler(this.decrypt_button_Click);
            // 
            // RC4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1224, 552);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.key);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.plaintext);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Name = "RC4";
            this.Text = "RC4";
            this.Load += new System.EventHandler(this.RC4_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox plaintext;
        private System.Windows.Forms.Button encryption_button;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox criptotext;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox key;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox original_text;
        private System.Windows.Forms.Button decrypt_button;
    }
}

