﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeySizeException
{
    public class KeySizeException:Exception
    {
        const string message = "Key size is insufficient; must be on 128 bits.";

        public KeySizeException() : base(message)
        {

        }
    }
}
