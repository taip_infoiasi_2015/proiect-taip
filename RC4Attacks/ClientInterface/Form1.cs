﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Encryption.Functional;
using System.Net.Sockets;
namespace ClientInterface
{
    public partial class RC4 : Form
    {

        public RC4()
        {
            InitializeComponent();
        }

        EncryptTranslationClient encryptTranslationClient = new EncryptTranslationClient();

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Text files | *.txt"; 
            dialog.Multiselect = false; 
            if (dialog.ShowDialog() == DialogResult.OK) 
            {
                String path = dialog.FileName; 
                label2.Text =  Path.GetFileName(path); ;
                var load = new Encryption.LoadFile(path);
                string[] result = load.LoadFromFile();
                plaintext.Text = String.Join(Environment.NewLine, result);
            }
        }

        private void encryption_button_Click(object sender, EventArgs e)
        {
            string result = GetDataEnc(this.key.Text.Trim(), this.plaintext.Text.Trim());
            criptotext.Text = result;
        }

        private void key_TextChanged(object sender, EventArgs e)
        {
            this.encryption_button.Enabled = !string.IsNullOrWhiteSpace(this.key.Text);
        }

        private void decrypt_button_Click(object sender, EventArgs e)
        {
            string result = GetDataDec(this.key.Text.Trim(), this.criptotext.Text.Trim());
            original_text.Text = Encryption.Functional.Encoding.FromHexString(result);
        }

        private string GetDataDec(string key, string source)
        {
            return encryptTranslationClient.DecryptData(key, source);
        }


        private string GetDataEnc(string key, string source)
        {
            return encryptTranslationClient.EncryptData(key, source);
        }


    }
}
