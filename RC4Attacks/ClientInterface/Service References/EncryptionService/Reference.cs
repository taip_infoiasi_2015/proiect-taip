﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ClientInterface.EncryptionService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="CompositeType", Namespace="http://schemas.datacontract.org/2004/07/RC4Service")]
    [System.SerializableAttribute()]
    public partial class CompositeType : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool BoolValueField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string StringValueField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool BoolValue {
            get {
                return this.BoolValueField;
            }
            set {
                if ((this.BoolValueField.Equals(value) != true)) {
                    this.BoolValueField = value;
                    this.RaisePropertyChanged("BoolValue");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string StringValue {
            get {
                return this.StringValueField;
            }
            set {
                if ((object.ReferenceEquals(this.StringValueField, value) != true)) {
                    this.StringValueField = value;
                    this.RaisePropertyChanged("StringValue");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="EncryptionService.EncryptTranslation")]
    public interface EncryptTranslation {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/EncryptTranslation/GetData", ReplyAction="http://tempuri.org/EncryptTranslation/GetDataResponse")]
        string GetData(int value);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/EncryptTranslation/GetData", ReplyAction="http://tempuri.org/EncryptTranslation/GetDataResponse")]
        System.Threading.Tasks.Task<string> GetDataAsync(int value);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/EncryptTranslation/GetDataUsingDataContract", ReplyAction="http://tempuri.org/EncryptTranslation/GetDataUsingDataContractResponse")]
        ClientInterface.EncryptionService.CompositeType GetDataUsingDataContract(ClientInterface.EncryptionService.CompositeType composite);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/EncryptTranslation/GetDataUsingDataContract", ReplyAction="http://tempuri.org/EncryptTranslation/GetDataUsingDataContractResponse")]
        System.Threading.Tasks.Task<ClientInterface.EncryptionService.CompositeType> GetDataUsingDataContractAsync(ClientInterface.EncryptionService.CompositeType composite);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/EncryptTranslation/EncryptData", ReplyAction="http://tempuri.org/EncryptTranslation/EncryptDataResponse")]
        string EncryptData(string key, string source);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/EncryptTranslation/EncryptData", ReplyAction="http://tempuri.org/EncryptTranslation/EncryptDataResponse")]
        System.Threading.Tasks.Task<string> EncryptDataAsync(string key, string source);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/EncryptTranslation/DecryptData", ReplyAction="http://tempuri.org/EncryptTranslation/DecryptDataResponse")]
        string DecryptData(string key, string source);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/EncryptTranslation/DecryptData", ReplyAction="http://tempuri.org/EncryptTranslation/DecryptDataResponse")]
        System.Threading.Tasks.Task<string> DecryptDataAsync(string key, string source);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface EncryptTranslationChannel : ClientInterface.EncryptionService.EncryptTranslation, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class EncryptTranslationClient : System.ServiceModel.ClientBase<ClientInterface.EncryptionService.EncryptTranslation>, ClientInterface.EncryptionService.EncryptTranslation {
        
        public EncryptTranslationClient() {
        }
        
        public EncryptTranslationClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public EncryptTranslationClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public EncryptTranslationClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public EncryptTranslationClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public string GetData(int value) {
            return base.Channel.GetData(value);
        }
        
        public System.Threading.Tasks.Task<string> GetDataAsync(int value) {
            return base.Channel.GetDataAsync(value);
        }
        
        public ClientInterface.EncryptionService.CompositeType GetDataUsingDataContract(ClientInterface.EncryptionService.CompositeType composite) {
            return base.Channel.GetDataUsingDataContract(composite);
        }
        
        public System.Threading.Tasks.Task<ClientInterface.EncryptionService.CompositeType> GetDataUsingDataContractAsync(ClientInterface.EncryptionService.CompositeType composite) {
            return base.Channel.GetDataUsingDataContractAsync(composite);
        }
        
        public string EncryptData(string key, string source) {
            return base.Channel.EncryptData(key, source);
        }
        
        public System.Threading.Tasks.Task<string> EncryptDataAsync(string key, string source) {
            return base.Channel.EncryptDataAsync(key, source);
        }
        
        public string DecryptData(string key, string source) {
            return base.Channel.DecryptData(key, source);
        }
        
        public System.Threading.Tasks.Task<string> DecryptDataAsync(string key, string source) {
            return base.Channel.DecryptDataAsync(key, source);
        }
    }
}
