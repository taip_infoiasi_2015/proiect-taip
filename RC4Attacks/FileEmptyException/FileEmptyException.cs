﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileEmptyException
{
    public class FileEmptyException : Exception
    {
        const string message = "The file you chose is empty. Nothing to decrypt!";

        public FileEmptyException():base(message)
        {

        }
    }
}
