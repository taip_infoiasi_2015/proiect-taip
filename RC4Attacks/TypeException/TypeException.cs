﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypeException
{
    public class TypeException:Exception
    {
        const string message = "File type chosen is inconsistent with the type required. (*.cap, *.pcap, *.ivs, *.dump )";

        public TypeException():base(message)
        {
           
        }
    }
}
