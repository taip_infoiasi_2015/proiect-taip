﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EncryptionTests
{
    [TestClass]
    public class LoadTests
    {
        [TestMethod]
        public void LoadDataFile()
        {
            string filePath = @"E:\TAIP\RC4Attacks\File\testfile.txt";
            var load = new Encryption.LoadFile(filePath);
            string[] result = load.LoadFromFile();

            string[] expectedresult = { "AAAAAA" };

            Assert.AreEqual(expectedresult[0], result[0]);
        }
    }
}
