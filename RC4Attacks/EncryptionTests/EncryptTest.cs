﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace EncryptionTests
{
    [TestClass]
    public class EncryptTest
    {
        [TestMethod]
        public void TestEncodingCharToUTF8()
        {
            char c = 'A';
            byte[] UTF8Data = Encryption.Functional.Encoding.ToUTF8(c);
            Assert.AreEqual(UTF8Data[0], 65);
        }

        [TestMethod]
        public void TestEncodingArrayToUTF8()
        {
            char[] arrayChar = { 'A', 'A', 'A' };
            byte[] expectedResult = { 65, 65, 65 };
            byte[] UTF8Data = Encryption.Functional.Encoding.ToUTF8(arrayChar);
            CollectionAssert.AreEqual(UTF8Data, expectedResult);
        }

        [TestMethod]
        public void TestEncodingStringToUTF8()
        {
            string data = "A";
            byte[] expectedResult = { 65 };
            byte[] UTF8Data = Encryption.Functional.Encoding.ToUTF8(data);
            CollectionAssert.AreEqual(UTF8Data, expectedResult);
        }

        [TestMethod]
        public void TestEncodingFromUTF8()
        {
            string data = "A";
            byte[] expectedResultbyte = { 65 };
            byte[] UTF8Data = Encryption.Functional.Encoding.ToUTF8(data);

            string expectedResult = "A";
            string result = Encryption.Functional.Encoding.FromUTF8(UTF8Data);
            Assert.AreEqual(result, expectedResult);

        }

        [TestMethod]
        public void EncryptdataTest()
        {
            string data, pwd;
            pwd = "4d6164616c696e61";
            data = "Madalina";


            var encryption = new Encryption.Encryption_obsolete();
            
            byte[] expectedBytes = { 0xBE, 0x54, 0x02, 0x8F, 0xA8, 0x24, 0x94, 0xC3 };

            string eHex = encryption.Encrypt(pwd, data, true);
            
            Assert.AreEqual(eHex, Encoding.GetEncoding(1252).GetString(expectedBytes));
        }
        

        [TestMethod]
        public void EncryptdataTest2()
        {
            String clar = "Madalina";
            String key = "4d6164616c696e61";
            byte[] expectedBytes = { 0x97, 0x0B, 0x8B, 0x35, 0x5A, 0xEF, 0x92, 0x94 };
            var encryption = new Encryption.Encryption_obsolete();

            string eHex = encryption.Encrypt(key, clar, false);
            
            Assert.AreEqual(eHex, Encoding.GetEncoding(1252).GetString(expectedBytes));
        }
        [TestMethod]
        public void EncryptdataTest3()
        {
            String clar = "Clar";
            String key = "123";
            byte[] expectedBytes = { 0x10, 0x9C, 0xDE, 0xF0 };
            var encryption = new Encryption.Encryption_obsolete();

            string eHex = encryption.Encrypt(key, clar, false);

            Assert.AreEqual(eHex, Encoding.GetEncoding(1252).GetString(expectedBytes));
        }

        [TestMethod()]
        public void LoadTestEncrypt()
        {
            Stopwatch watch = new Stopwatch();
            string text = System.IO.File.ReadAllText(@"C:\Facultate\proiect-taip\KleinBranch\KleinAttackCode\bin\Debug\test_file.txt");
            String key = "123";
            Encryption.Encryption_obsolete encryption = new Encryption.Encryption_obsolete();
            watch.Start();
            String result = encryption.Encrypt(text, key, false);
            watch.Stop();
            Console.WriteLine("Time elapsed: {0}", watch.Elapsed);
            Assert.IsTrue(watch.ElapsedMilliseconds < 500);
        }
    }
}
