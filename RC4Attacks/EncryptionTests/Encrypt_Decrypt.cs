﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Encryption;
using System.IO;
using Encryption.Functional;
using System.Diagnostics;
namespace EncryptionTests
{
    [TestClass]
    public class Encrypt_Decrypt
    {
        [TestMethod]
        public void TestAllCases()
        {
            TestAll();
        }

        public  void TestAll()
        {
            String[,] testCase =
            {
                { "0123456789abcdef0123456789abcdef", "0123456789abcdef", "75b7878099e0c596"},
                { "0123456789abcdef", "0000000000000000", "7494c2e7104b0879"},
                { "0000000000000000", "0000000000000000", "de188941a3375d3a"},
                { "ef012345", "00000000000000000000", "d6a141a7ec3c38dfbd61"},
                { "Key".ToUTF8Hex(), "Plaintext".ToUTF8Hex(), "BBF316E8D940AF0AD3"},
                { "Wiki".ToUTF8Hex(), "pedia".ToUTF8Hex(), "1021BF0420"},
                { "rc4.online-domain-tools.com".ToUTF8Hex(), "online-domain".ToUTF8Hex(), "fd28fadd4d249fee77969e0cc3328d1a8470b2c7df3d625c1602e7"},

            };

            int nTests = testCase.GetLength(0);
            int failed = 0;
            int tested = 0;

            for (int i = 0; i < nTests; i++)
            {
                tested++;
                if (!TestOne(testCase[i, 0], testCase[i, 1], testCase[i, 2], false))
                    failed++;

                tested++;
                if (!TestOne(testCase[i, 0], testCase[i, 1], testCase[i, 2], true))
                    failed++;

                tested++;
                if (!TestOneStream(testCase[i, 0], testCase[i, 1], testCase[i, 2]))
                    failed++;
            }
            Debug.WriteLine(" Tested = {0}, Failed = {1}, Success = {2}", tested, failed, failed == 0);

            //Console.ReadLine();
        }
        public bool TestOne(string key, string source, string expect, bool InSteps)
        {
            byte[] byteKey = key.FromHex();
            byte[] byteSource = source.FromHex();


            var rc4 = new Rc4ED(byteKey);

            if (!InSteps)
                rc4.Encrypt(byteSource);
            else
            {
                for (int i = 0; i < byteSource.Length; i++)
                    rc4.Encrypt(byteSource, i, 1);
            }

            var result = byteSource.ToHex();

            string decript = decrypt(key, result, InSteps);

            expect = expect.ToUpper();

            var success = decript.Equals(source.ToUpper());

            if (!success)
                Debug.WriteLine(" {0} key={1} src={2}, expect={3} got={4}"
                        , success ? "Ok  " : "Fail"
                        , key
                        , source
                        , expect
                        , result
                    );

            return success;
        }

        public  string decrypt(string key, string source, bool InSteps)
        {
            byte[] byteKey = key.FromHex();
            byte[] byteSource = source.FromHex();

            string result;
            var rc4 = new Rc4ED(byteKey);

            if (!InSteps)
                rc4.Encrypt(byteSource);
            else
            {
                for (int i = 0; i < byteSource.Length; i++)
                    rc4.Encrypt(byteSource, i, 1);
            }

            return result = byteSource.ToHex();
        }
        public bool TestOneStream(string key, string source, string expect)
        {
            byte[] byteKey = key.FromHex();

            var streamIn = new MemoryStream(source.FromHex());

            var streamOut = new MemoryStream();

            var rc4 = new Rc4ED(byteKey);

            rc4.Encrypt(streamIn, streamOut);

            byte[] encoded = new byte[source.Length >> 1];

            streamOut.Position = 0;

            int nRead = streamOut.Read(encoded, 0, encoded.Length);

            if (nRead != encoded.Length)
            {
                System.Diagnostics.Debug.Assert(false, "# of bytes read");
            }

            var result = encoded.ToHex();
            expect = expect.ToUpper();

            var success = expect.Equals(result);

            if (!success)
                Debug.WriteLine(" {0} key={1} src={2}, expect={3} got={4}"
                        , success ? "Ok  " : "Fail"
                        , key
                        , source
                        , expect
                        , result
                    );

            return success;

        }

    }
}
