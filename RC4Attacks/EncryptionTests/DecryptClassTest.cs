﻿using Encryption;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EncryptionTests
{
    [TestClass]
    public class DecryptClassTest
    {
        [TestMethod]
        public void testSwaping()
        {
            int a = 2;
            int b = 3;
            byte result;

            var decryptObject = new Decrypt();
            result = decryptObject.getNextKeyByte(a, b);

            Assert.AreEqual(1, result);
        }

        [TestMethod]
        public void testGenerateKey()
        {
            var decryptObject = new Decrypt();
            var randomNumber = decryptObject.generateKeyStream();
            Assert.IsNotNull(randomNumber);
        }
    }
}
